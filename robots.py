#!/usr/bin/python3
import argparse
import jinja2
import os
import requests
import urllib.robotparser


path = os.path.dirname(os.path.abspath(__file__))

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Small script to curl users robots.txt and agregate them in a single file')
	parser.add_argument('-o', '--output', help='Path to write the file to, - means stdout', default='/var/www/perso/robots.txt')
	args = parser.parse_args()

	# Get target list
	robots = {}
	targets = os.listdir('/home')
	for target in targets:
		robot_path = os.path.join('/home', target, 'www', 'robots.txt')
		if not os.path.exists(robot_path):
			continue
		rp = urllib.robotparser.RobotFileParser()
		with open(robot_path) as file:
			rp.parse(file)
		for entry in ([rp.default_entry] if rp.default_entry else []) + rp.entries:
			for agent in entry.useragents:
				for rule in entry.rulelines:
					rule_path = rule.path if rule.path.startswith('/') else f'/{rule.path}'
					if agent not in robots:
						robots[agent] = { True: [], False: [] }
					robots[agent][rule.allowance] += [ f'/{target}{rule_path}', f'/~{target}{rule_path}' ]

	# Dump generated robots to file (or stdout)
	with open(os.path.join(path, 'templates', 'robots.txt.j2')) as file:
		template = jinja2.Template(file.read())
	robots = template.render(robots=robots)

	if args.output == '-':
		print(robots)
	else:
		with open(args.output, 'w') as file:
			file.write(robots)
